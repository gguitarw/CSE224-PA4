# Programming Assignment 4

This assignment is to implement a GUI for a travel reservation system using bash and Zenity. 

## Instructions

The program should:
* Create a dialog box asking for the traveler's name
* Display a list of cities from which the traveler may depart (7 or more)
* Display the same list of cities to which the traveler may be heading
* Display a calendar dialog for when the traveler is leaving
* Display a calendar dialog for when the traveler is returning
* Ask the user how many bags they will be checking
* Display a summary of this information on the terminal

It will then do the following on the backend:
* Search a file named "reservations.txt" for the user's name.
 * If they already have a reservation, remove that reservation from the reservations.txt file
* Add the user's reservation to the reservations.txt file. It should be stored as a single line in the following format:
```name|departure city|destination city|departure date|arrival date|number of bags```

## Other details

This is a group project of 1-4 people. You should be prepared to present your project to the class on the due date.

The presentation should include:
* A demonstration of the operation of your program
* A walkthrough of the highlights of your code
* An explaination of how the work was divided amongst group members

Only one group member should submit the group's project.
