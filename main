#!/bin/bash
###################################################
#                                                 #
# Group Members: Garrett Walter and CJ Pfenninger #
# Course:        CSE 224 - Programming Tools      #
# Date:          2017-11-02 (updated 2017-11-16)  #
# Assignment:    Program 4                        #
#                                                 #
###################################################

#-----------Global Variables & Init-----------#
name=""       # user's name
cityDepart="" # leaving this place
cityArrive="" # destination
dateDepart="" # YYYY-MM-DD of departure
dateReturn="" # YYYY-MM-DD of return
bagCount=""   # number of bags
confirmKey=0  # used to keep the user in the script if their information is not confirmed
locNum=0      # keeps track of location array element
# "valid" variables are used to break/maintain the while loops surrounding each user input section based on users inputs
validLocation=0 # 0 if departure/arrival locations are unchecked/incorrect, 1 if valid
validDate=0 # 0 if departure/return dates are unchecked/incorrect, 1 if valid
validName=0 # 0 if name is empty, 1 if valid
englishDepart="" # used to display date in easier to read format in confirmReservation()
englishReturn="" # used to display date in easier to read format in confirmReservation()
errMessage="" # used to hold the required error message : see case in errPrompt()
TODAY=`date +%F` # holds the current epoch time for 00:00:00 of the current date for conditionals relating to time-traveling users

#-----------Zenity Config----------#
calW=200
calH=350

#-----------Functions--------------#
addReservation()
{
# This function is called from confirmReservation after the user clicks yes to confirm the information is correct.
# grep looks for the name the user entered.
# if grep finds the name, we use sed to overwrite the previous entry that corresponds to the name entered by the user
# if grep does not find a match, we append the user's information to the end of the file

	grep -n "^$name|" reservations.txt > /dev/null # send output to null
	
	if [[ $? -eq 0 ]]
	then #there is already an entry for $name, replace current reservation with updated reservation
		sed -i s/^"$name"\|.*/"$name\|$cityDepart\|$cityArrive\|$dateDepart\|$dateReturn\|$bagCount"/ reservations.txt
	else #there was no entry for name, add reservation to end of file
		echo "$name|$cityDepart|$cityArrive|$dateDepart|$dateReturn|$bagCount" >> reservations.txt
	fi

	zenity --title="Reservation Added"\
		--info --ellipsize\
		--text="Your reservation has been added.  Thank you."\
		|& grep -v Gtk-Message
}

confirmReservation()
{
# This function converts the date entered by the user into an easy to read format
# {PIPESTATUS[0]} will contain the exit status for the zenity command
# If the user clicks yes to confirm, call addReservation
# if the user clicks no, they will be taken back to the beginning of getInformation, and the "valid" variables are reset to init value 

	englishDepart=`date -d "$dateDepart" "+%B %d, %Y"`
	englishReturn=`date -d "$dateReturn" "+%B %d, %Y"`
	zenity --title="Confirm Reservation"\
		--question --ellipsize\
		--text="Please confirm the following information is correct:\nName = $name\nDeparture City = $cityDepart\nArrival City = $cityArrive\nDeparture Date = $englishDepart\nReturn Date = $englishReturn\nBag Count = $bagCount"\
		|& grep -v Gtk-Message
	
	if [[ ${PIPESTATUS[0]} -eq 0 ]]
	then 	# user clicked yes
		addReservation
		confirmKey=1
	else 	# user clicked no
		validLocation=0
		validDate=0
		validName=0
	fi
}

checkLocation()
{
# This function checks to make sure the user is not attempting to Anti-Travel
# If an error message is displayed, the cityDepart and cityArrive variables will be reset to init value
# leaving the user's input in these variables gives them the ability to confirm an impossible travel booking
# if the user's selections are valid, then set validLocation to 1 to move on

	if [[ "$cityDepart" == "$cityArrive" ]]
	then	# show error and empty the variables
		errPrompt 2
		cityDepart=""
		cityArrive=""
	else
		validLocation=1
	fi
}

checkDate()
{ 
# This function checks to make sure the user it not attempting to Time-Travel. 
# use date -d YYYY-MM-DD +%s to get unix epoch time, and compare that for the given dates
# if an error message is displayed, the dateDepart and dateReturn variables will be reset to init value
# leaving the user's input in these variables gives them the ability to confirm an impossible travel booking
# if the user's selections are valid, then set validLocation to 1 to move on

	epochDepart=`date -d "$dateDepart" +%s`
	epochReturn=`date -d "$dateReturn" +%s`
	if (( $epochReturn < $epochDepart || $epochDepart < `date -d "$TODAY" +%s` ))
	then
		errPrompt 1
		dateDepart=""
		dateReturn=""
	else
		validDate=1
	fi
}

checkCancel()
{
# This function detects if the user clicked cancel or okay
# if the user clicks cancel, call errPrompt() and exit
# otherwise proceed

	if [[ ${PIPESTATUS[0]} -ne 0 ]]
	then #user clicked "CANCEL", exit the program
		errPrompt
		exit
	fi #otherwise, user clicked "OKAY"
}

cityList()
{
	while read line #get input from locationlist.txt, read each line into the variable $line
	do
		#fill every other array value with a blank, those will be radio buttons
		location[$locNum]=""
		locNum=$(($locNum+1))
		#fill the rest with the lines from locationlist.txt
		location[$locNum]="$line"
		locNum=$(($locNum+1))
	done <<< "$(cat locationlist.txt)"
}

getInformation()
{ 
# This function displays the necessary prompts to get information from the user
# Ask user for name, departure city, arrival city, departure date, return date, and number of travel bags
# 
	
	# Create dialog box asking for travelers name  (use zenity --entry)
	while [[ $validName -eq 0 ]]
	do
		name=$(zenity --title="Name"\
				--entry --text="Enter your Name."\
				|& grep -v Gtk-Message)
		checkCancel
		if [[ $name != "" ]] #only continue if the name isn't blank
		then
			validName=1
		fi
	done

	while [[ $validLocation -eq 0 ]]
	do
		# Display a list of cities from which the traveler may depart
		cityDepart=$(zenity --title="Enter Departure City"\
					--list --radiolist --text=""\
					--column="" --column="Departing From"\
					"${location[@]}"\
					--width=$calW --height=$calH \
					|& grep -v Gtk-Message)
		checkCancel

		# Display the same list of cities where the traveler may go
		cityArrive=$(zenity --title="Enter Arrival City"\
					--list --radiolist --text=""\
					--column="" --column="Arriving In"\
					"${location[@]}"\
					--width=$calW --height=$calH \
					|& grep -v Gtk-Message)
		checkCancel
		checkLocation
	done

	while [[ $validDate -eq 0 ]]
	do
		# Display a calendar dialog for leaving
		dateDepart=$(zenity --title="Enter Departure Date"\
					--calendar --date-format=%F\
					|& grep -v Gtk-Message)
		checkCancel

		# Display a calendar dialog for returning
		dateReturn=$(zenity --title="Enter Return Date"\
					--calendar --date-format=%F\
					|& grep -v Gtk-Message)
		checkCancel
		checkDate
	done

	# Ask the user how many bags they're bringing (from 0 to 5)
	bagCount=$(zenity --title="Bag Count"\
			--scale\
			--text="How many bags will you bring?"\
			--value=1 --max-value=5\
			|& grep -v Gtk-Message)
        checkCancel
}

errPrompt()
{ #case EXPRESSION in CASE1) command ;; CASE2) commands ;; CASEn)
	# This function will contain error message prompts.  Use command arguments to pick the appropriate message.
	# Pass in the following arguments for the error message needed
	# No arg - default error
	# 1  -  Time travel
	# 2  -  Anti-travel
	
	case $1 in
		1) #Time Travel
			errMessage="Error: You entered an invalid date.\nMake sure your return date is after your departure date, or your departure date has not already passed."
		;;
		2) #Anti Travel
			errMessage="Error: You entered an invalid flight schedule.\nMake sure you are actually going somewhere."
		;;
		*) #default error
			errMessage="Exiting program."
		;;
	esac

	zenity --title="Error" --error --ellipsize --text="<span foreground=\"red\" font=\"12\"><b>$errMessage</b></span>\n\n"\
		|& grep -v Gtk-Message
}

#-----------Main Function------------#
cityList #initialize list of locations

while [[ $confirmKey -eq 0 ]]
do
	getInformation
	confirmReservation
done
